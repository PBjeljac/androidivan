package com.example.sensormanager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executor;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.sensormanager.readers.AbstractSensorReader;

public class SensorReaderManager implements Runnable {

	Context context;
	
	private HashMap<String, AbstractSensorReader> registeredSensors;
	private ArrayList<String> sentToSensors; 
	//private ArrayList<String> responceFromSensors;
	
	
	public SensorReaderManager(Context c) {
		//Timestamp t = new Timestamp(new Date().getTime());
		
		context = c;
		registeredSensors = SensorReaderFactory.buildSensorReaders(context, this);
	}
	
	public void registerSensor(String name, AbstractSensorReader reader){
		
		if(registeredSensors == null)
			registeredSensors = new HashMap<String, AbstractSensorReader>();
		registeredSensors.put(name, reader);
		
	}
	
	public void startSensors(){
		
		Iterator it = registeredSensors.entrySet().iterator();
		
		while(it.hasNext()){
			Map.Entry pair = ((Map.Entry)it.next());
			try {
				new ThreadedSensor().execute(((AbstractSensorReader)pair.getValue()));
			} catch (Exception e) {
				//skip
			}finally{
				if(sentToSensors == null)
					sentToSensors = new ArrayList<String>();
				sentToSensors.add((String)pair.getKey());
			}
			
		}
	}
	
	public void responseFromSensor(String key, String message, Timestamp t){
		Log.d("RESPONCE ","component " + key + " timestamp " + t.toString() + " message " + message);
		sentToSensors.remove(key);
		if(sentToSensors.size()==0)
			Log.d("ALL RECEIVED","FULL PACKAGE");
	}
	
	@Override
	public void run() {
		startSensors();
		
	}
	
	private class ThreadedSensor implements Executor{
		
		@Override
		public void execute(Runnable command) { 
			
			
			
			new Thread(command).run();
			Log.d("Thread number:", Integer.toString(Thread.activeCount()));
		}

		
	}
	
}
