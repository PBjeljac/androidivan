package com.example.sensormanager.readers;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.example.sensormanager.SensorReaderManager;

public class WirelessSensorReader extends AbstractSensorReader {

	private WifiManager mainWifi;
	private WifiReceiver receiverWifi;

	private List<ScanResult> wifiList;

	public WirelessSensorReader(String key, Context c, SensorReaderManager srm) {
		super(key, c, srm);
		// TODO Auto-generated constructor stub
	}

	boolean enabled = true;

	@Override
	public void run() {

		Log.d("StartedThread", key);

		doAction();

	}

	private void doAction() {
		// Log.d("WIFI", "Pozvan WIFI");

		mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		receiverWifi = new WifiReceiver();
		context.registerReceiver(receiverWifi, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		enabled = mainWifi.isWifiEnabled();
		if (!enabled) {
			mainWifi.setWifiEnabled(true);
		}
		mainWifi.startScan();

	}

	class WifiReceiver extends BroadcastReceiver {
		public void onReceive(Context c, Intent intent) {
			try {
				wifiList = mainWifi.getScanResults();

				// Log.d("WIFI", "RECEIVER");
				// Log.d("WIFI", Integer.toString(wifiList.size()));

				// Intent msgIntent = new Intent(context,
				// WifiIntentService.class);
				// msgIntent.putExtra(WifiIntentService.PARAM_IN_MSG,
				// strInputMsg);
				// context.startService(msgIntent);

				new Runnable() {

					@Override
					public void run() {
						String lista = "";
						for (int i = 0; i < wifiList.size(); i++) {
							lista += wifiList.get(i).SSID + ",  ";
						}

						manager.responseFromSensor(key, lista, new Timestamp(
								new Date().getTime()));
						lista = "";

						// Log.d("WIFILIST", lista);

					}
				}.run();

				mainWifi.setWifiEnabled(enabled);

				context.unregisterReceiver(receiverWifi);
			} catch (Exception e) {
				Log.e("WIFIERROR", "PUCE NESTO");
			}

		}
	}

}
