package com.example.sensormanager.readers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Looper;
import android.util.Log;

import com.example.sensormanager.SensorReaderManager;

public class BluetoothSensorReader extends AbstractSensorReader {

	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothReceiver receiverBT;
	private List<BluetoothDevice> btList;
	private String message = "";
 
	private boolean enabled = true;
	private boolean first = true;

	public BluetoothSensorReader(String key, Context c, SensorReaderManager srm) {
		super(key, c, srm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		doAction();
	}

	private void doAction() {

		Log.d("StartedThread", key);

		Looper.prepare();

		receiverBT = new BluetoothReceiver();

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			Log.e("BLUETOOTH", "NO BLUETOOTH ON DEVICE");
		}
		try {
			if (first)
				enabled = mBluetoothAdapter.isEnabled();
			first = false;

			if (!enabled) {
				mBluetoothAdapter.enable();
			}
		} catch (Exception e) {
			//Log.e("BLUETOOTH", "NIJE USPELO UKLJUCIVANJE");
			//Log.e("EXC", e.getMessage());
		}

		if (mBluetoothAdapter.isDiscovering()) {
			Log.d("Discovery", "already on");
			mBluetoothAdapter.cancelDiscovery();
		}

		mBluetoothAdapter.startDiscovery();

		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);

		context.registerReceiver(receiverBT, filter);
	}

	class BluetoothReceiver extends BroadcastReceiver {
		public void onReceive(Context c, Intent intent) {
			// Log.d("BLUETOOTH", "BT broadcast");
			String action = intent.getAction();
			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Get the BluetoothDevice object from the Intent
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				// Add the name and address to an array adapter to show in a
				// ListView
				if (btList == null)
					btList = new ArrayList<BluetoothDevice>();
				btList.add(device);
				message += device.getName() + "    ";
				// Log.d("BLUETOOTH", "BT " + device.getName());
			}

			if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				// Log.d("FINISHED", "FINISHED");
				context.unregisterReceiver(receiverBT);

				if (!enabled) {
					// Log.d("BT", "DISABLE");
					mBluetoothAdapter.disable();
				}

				manager.responseFromSensor(key, message, new Timestamp(
						new Date().getTime()));
				message = "";

			}

		}
	}

}
