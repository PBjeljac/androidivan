package com.example.sensormanager.readers;

import java.sql.Timestamp;

import android.content.Context;

import com.example.sensormanager.SensorReaderManager;

public abstract class AbstractSensorReader implements Runnable {

	public AbstractSensorReader(String key, Context c, SensorReaderManager srm) {
		context = c;
		this.key = key;
		manager = srm;
	}

	Context context;
	SensorReaderManager manager;
	Timestamp time;

	String key;
		
	public boolean start(Timestamp t){
		try {
			time = t;
			
			run();
			
			return true;
		} catch (Exception e) {
			return false;
		}
		
		
	}

}
