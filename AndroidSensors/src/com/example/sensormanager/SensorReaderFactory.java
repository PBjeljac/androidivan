package com.example.sensormanager;

import java.util.HashMap;

import android.content.Context;

import com.example.sensormanager.readers.AbstractSensorReader;
import com.example.sensormanager.readers.BluetoothSensorReader;
import com.example.sensormanager.readers.WirelessSensorReader;

public class SensorReaderFactory {

	
	public static HashMap<String, AbstractSensorReader> buildSensorReaders(Context c, SensorReaderManager srm){
		
		HashMap<String, AbstractSensorReader> sensorReaders = new HashMap<String, AbstractSensorReader>();
		
		sensorReaders.put("wireless", new WirelessSensorReader("wireless", c, srm));
		sensorReaders.put("bluetooth", new BluetoothSensorReader("bluetooth", c, srm));
		
		return sensorReaders;
		
	}
	
}
